/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Melon
 */
public class Frame1 {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // frame.setSize(new Dimension(500,300));
        frame.setSize(500, 300); //Overloading 
        
        JLabel lblHelloWorld = new JLabel("Hello World!", JLabel.CENTER);
        lblHelloWorld.setBackground(Color.ORANGE);
        lblHelloWorld.setOpaque(true);
        lblHelloWorld.setFont(new Font("Verdana", Font.PLAIN, 25));
        frame.add(lblHelloWorld);
        
        frame.setVisible(true);
    }
}
